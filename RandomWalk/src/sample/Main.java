package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.text.Text;

import java.util.Random;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Button button = new Button("Получить");
        TextField textField = new TextField();
        textField.setPrefSize(100, 10);
        Text[] arrText = new Text[3];
        arrText[0] = new Text("Случайные блуждания");
        arrText[1] = new Text("Количество блужданий");
        arrText[2] = new Text("Результат:");
        button.setOnAction(actionEvent -> {
            int result = 0;
            try{
                result = this.randomWalk(Integer.parseInt(textField.getText()));
                arrText[2].setText("Результат: " + String.valueOf(result));
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                arrText[2].setText("Результат: Invalid input");
            }
        });
        VBox vbox = new VBox(button);
        vbox.setPadding(new Insets(0, 10, 10, 10));
        vbox.setSpacing(10);
        vbox.setAlignment(Pos.CENTER);
        GridPane gridPane = new GridPane();
        gridPane.add(arrText[1], 0, 0);
        gridPane.add(textField, 1, 0);
        gridPane.add(arrText[2], 0, 1);
        gridPane.add(button, 1, 1);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        vbox.getChildren().addAll(arrText[0], gridPane);
        Scene scene = new Scene(vbox, 300, 100);

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private int randomWalk(int count_of_steps) throws Exception{
        Random random = new Random();
        int position = 0;
        for (;count_of_steps > 0; --count_of_steps){
            position += (random.nextInt(2) == 1) ? 1 : -1;
        }

        return position;
    }
    public static void main(String[] args) {
        launch(args);
    }
}
